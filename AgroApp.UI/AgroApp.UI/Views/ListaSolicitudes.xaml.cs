﻿using AgroApp.UI.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace AgroApp.UI.Views
{
    /// <summary>
    /// Page showing the data table
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListaSolicitudes : ContentPage
    {
        public ListaSolicitudes()
        {
            this.InitializeComponent();
            this.BindingContext = ListaSolicitudesViewModel.BindingContext;
        }
    }
}