﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AgroApp.UI.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactEmailEntry : ContentView
    {
        public ContactEmailEntry()
        {
            this.InitializeComponent();
        }
    }
}