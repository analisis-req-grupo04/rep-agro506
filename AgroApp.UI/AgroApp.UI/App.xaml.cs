using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: ExportFont("Montserrat-Bold.ttf",Alias="Montserrat-Bold")]
     [assembly: ExportFont("Montserrat-Medium.ttf", Alias = "Montserrat-Medium")]
     [assembly: ExportFont("Montserrat-Regular.ttf", Alias = "Montserrat-Regular")]
     [assembly: ExportFont("Montserrat-SemiBold.ttf", Alias = "Montserrat-SemiBold")]
     [assembly: ExportFont("UIFontIcons.ttf", Alias = "FontIcons")]
namespace AgroApp.UI
{
    public partial class App : Application
    {
        public static string ImageServerPath { get; } = "https://cdn.syncfusion.com/essential-ui-kit-for-xamarin.forms/common/uikitimages/";
        public App()
        {
            //Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NDc4NTAwQDMxMzkyZTMyMmUzMEkwaFArYUxEYmcvNUNXZVVVc0Z0NzF4TUd5MnU2WXhsU1h5ckZOS2tpUVU9");
            InitializeComponent();

            NavigationPage navPage =
            new NavigationPage(new Views.SimpleLoginPage());
            MainPage = navPage;
            //MainPage = new Views.SimpleLoginPage();
            //MainPage = new Views.HomePage();
            //MainPage = new Views.GenerarReceta();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
