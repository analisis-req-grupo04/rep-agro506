﻿using Xamarin.Forms.Xaml;

namespace AgroApp.UI.Styles
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AvatarViewStyles
    {
        public AvatarViewStyles()
        {
            this.InitializeComponent();
        }
    }
}