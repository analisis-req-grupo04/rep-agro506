# PROYECTO AGRO 506 #

Este repositorio corresponde al proyecto de la aplicación movil de la compañia COSEINCA

Nombre del Proyecto: Agro 506

### Integrantes: ###

	* Asdrúbal Torres Siles
	* Carlo Magno Gutierrez
	* Alexander Villegas Valverde

### Descripción ###

La aplicación Agro 506 consta de un sistema encargado de generar solicitudes de atención por parte de los clientes en caso de que alguno de sus cultivos presente algún daño y asi poder llevar un 
seguimiento de dicha solicitud la cual será recibida por un ingeniero agrónomo para su análisis y así dictar una solución para la recuperación de dicho cultivo.

### Instalación ###

Paso 1: Instalar el software de Git SCM para Windows mediante el siguiente enlace: https://gitforwindows.org, en él únicamente seleccionaremos la opción "Download".
Paso 2: Una vez descargado ejecutaremos el software y para su instalacción unicamente daremos "Next" hasta finalizar su instalación.
Paso 3: Crearemos una carpeta en donde situaremos nuestro proyecto, daremos click derecho dentro de la carpeta y seleccionaremos la opción "Git Bash Here" el cual nos abrirá una especie de consola.
Paso 4: En dicha consola pegaremos el siguiente comando y daremos "Enter": git clone https://analisis-req-grupo04-admin@bitbucket.org/analisis-req-grupo04/rep-agro506.git
Y en cuestión de segundos debemos de observar como nuestro repositorio es creado dentro de la carpeta escogida :)
